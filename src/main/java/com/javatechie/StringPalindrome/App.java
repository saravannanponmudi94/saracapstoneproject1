package com.javatechie.StringPalindrome;

/**
 * Hello world!
 *
 */
public class App {

	public boolean isPalindrome(String input) {

		if (input == null) {
			throw new IllegalArgumentException("input shouldn't be null");
		}

		if (input.equals(reverse(input))) {
			System.out.println("Input is a palindrome");
			return true;
		} else {
			System.out.println("Input is not a palindrome");
			return false;
		}
	}

	private String reverse(String input) {
		String rev = "";
		for (int i = input.length() - 1; i >= 0; i--) {
			rev = rev + input.charAt(i);
		}
		return rev;
	}

}
